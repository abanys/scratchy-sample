package net.banys.scratchysample;

import android.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.banys.scratchysample.bus.BusProvider;
import net.banys.scratchysample.bus.event.ShowAlertEvent;

public class MainActivity extends AppCompatActivity {

    private Bus mBus = BusProvider.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        mBus.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onShowAlertEvent(ShowAlertEvent event) {
        new AlertDialog.Builder(this)
                .setTitle("Alert")
                .setMessage(event.getText())
                .setPositiveButton("OK", null)
                .show();
    }

}
