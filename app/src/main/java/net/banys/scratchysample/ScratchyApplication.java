package net.banys.scratchysample;

import android.app.Application;

import com.squareup.otto.Bus;

import net.banys.scratchysample.bus.BusProvider;
import net.banys.scratchysample.data.DataManager;

/**
 * Created by adam on 22.12.15.
 */
public class ScratchyApplication extends Application {

    private Bus mBus = BusProvider.getInstance();
    private DataManager mDataManager = new DataManager();

    @Override
    public void onCreate() {
        super.onCreate();
        mBus.register(mDataManager);
    }

}
