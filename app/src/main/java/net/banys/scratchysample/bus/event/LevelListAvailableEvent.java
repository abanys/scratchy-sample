package net.banys.scratchysample.bus.event;

import net.banys.scratchysample.model.Level;

import java.util.List;

/**
 * Created by adam on 22.12.15.
 */
public class LevelListAvailableEvent {

    private List<Level> mLevels;

    public LevelListAvailableEvent(List<Level> levels) {
        mLevels = levels;
    }

    public List<Level> getLevels() {
        return mLevels;
    }

}
