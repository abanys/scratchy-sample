package net.banys.scratchysample.bus.event;

/**
 * Created by adam on 23.12.15.
 */
public class ShowAlertEvent {

    private String text;

    public ShowAlertEvent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }
}
