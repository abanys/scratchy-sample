package net.banys.scratchysample.data;

import com.squareup.otto.Produce;

import net.banys.scratchysample.bus.event.LevelListAvailableEvent;
import net.banys.scratchysample.data.level.LevelProvider;
import net.banys.scratchysample.data.level.MockLevelProvider;

/**
 * Created by adam on 22.12.15.
 */
public class DataManager {

    private LevelProvider mLevelProvider;

    public DataManager() {
        mLevelProvider = new MockLevelProvider();
    }

    @Produce
    public LevelListAvailableEvent produceAnswer() {
        // assuming this data will always exists
        return new LevelListAvailableEvent(mLevelProvider.getLevels());
    }

}
