package net.banys.scratchysample.data.level;

import net.banys.scratchysample.model.Level;

import java.util.List;

/**
 * Created by adam on 22.12.15.
 */
public interface LevelProvider {

    public List<Level> getLevels();

}
