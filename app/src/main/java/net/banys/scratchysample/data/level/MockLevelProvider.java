package net.banys.scratchysample.data.level;

import net.banys.scratchysample.R;
import net.banys.scratchysample.model.Level;

import java.util.ArrayList;
import java.util.List;

/**
 * Sample level provider which generates mock level elem
 * Created by adam on 22.12.15.
 */
public class MockLevelProvider implements LevelProvider {

    private static final int LEVEL_CNT = 10;

    private static final int[] BG_COLORS = new int[] {
            R.color.level1, R.color.level2, R.color.level3, R.color.level4, R.color.blue
    };

    @Override
    public List<Level> getLevels() {
        List<Level> result = new ArrayList<>(LEVEL_CNT);
        for (int i=1; i<=LEVEL_CNT; i++)
            result.add(new Level(i, 200*i, 5*i, i==1, BG_COLORS[i%5]));
        return result;
    }

}
