package net.banys.scratchysample.levels;

import android.content.Context;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.ShapeDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import net.banys.scratchysample.R;
import net.banys.scratchysample.bus.BusProvider;
import net.banys.scratchysample.bus.event.ShowAlertEvent;
import net.banys.scratchysample.model.Level;
import net.banys.scratchysample.utils.ViewHelper;

import java.util.List;

/**
 * Created by adam on 23.12.15.
 */
public class LevelListAdapter extends RecyclerView.Adapter<LevelListAdapter.ViewHolder> {

    private List<Level> mLevels;
    private Context mContext;

    public LevelListAdapter(Context context, List<Level> levels) {
        mContext = context;
        mLevels = levels;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext().getApplicationContext())
                .inflate(R.layout.list_item_level, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        holder.container.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.level != null) {
                    BusProvider.getInstance().post(new ShowAlertEvent(holder.level.isUnlocked() ?
                    "Level Unlocked" : "Level Locked"));
                }
            }
        });
        holder.viewRewards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (holder.level != null) {
                    BusProvider.getInstance().post(
                            new ShowAlertEvent("View rewards for level: " + holder.level.getId()));
                }
            }
        });
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Level level = mLevels.get(position);
        holder.level = level;
        holder.levelPosition.setText(String.valueOf(level.getId()));
        ViewHelper.setBackgroundShapeColor(mContext, holder.levelPosition, level.getBackground());
        holder.levelTitle.setText(mContext.getString(R.string.list_item_level_header)
                + " " + level.getId());

        if (level.isUnlocked()) {
            holder.lockedView.setVisibility(View.GONE);
            holder.unlockedView.setVisibility(View.VISIBLE);
            holder.scratchesPerDayLabel.setVisibility(View.INVISIBLE);
            holder.scratchesPerDayValue.setVisibility(View.INVISIBLE);
            holder.pointsToUnlockLabel.setVisibility(View.INVISIBLE);
            holder.pointsToUnlockValue.setText(R.string.list_item_level_unlocked);
        }
        else  {
            holder.lockedView.setVisibility(View.VISIBLE);
            holder.unlockedView.setVisibility(View.GONE);
            holder.scratchesPerDayLabel.setVisibility(View.VISIBLE);
            holder.scratchesPerDayValue.setVisibility(View.VISIBLE);
            holder.pointsToUnlockLabel.setVisibility(View.VISIBLE);
            holder.scratchesPerDayValue.setText(String.valueOf(level.getScratchesPerDay()) + " ");
            holder.pointsToUnlockValue.setText(String.valueOf(level.getPointsToUnlock()) + " " +
                    mContext.getString(R.string.list_item_points) + " ");
        }

        // set line visibility
        if (position == 0) {
            holder.line.setVisibility(View.GONE);
            holder.lineFirstItem.setVisibility(View.VISIBLE);
            holder.lineLastItem.setVisibility(View.GONE);
        } else if (position == getItemCount()-1) {
            holder.line.setVisibility(View.GONE);
            holder.lineFirstItem.setVisibility(View.GONE);
            holder.lineLastItem.setVisibility(View.VISIBLE);
        } else {
            holder.line.setVisibility(View.VISIBLE);
            holder.lineFirstItem.setVisibility(View.GONE);
            holder.lineLastItem.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mLevels.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        Level level;
        View container;
        TextView levelPosition;
        TextView levelTitle;
        TextView viewRewards;
        TextView pointsToUnlockValue;
        TextView pointsToUnlockLabel;
        TextView scratchesPerDayValue;
        TextView scratchesPerDayLabel;
        View unlockedView;
        View lockedView;
        View line;
        View lineFirstItem;
        View lineLastItem;

        public ViewHolder(View itemView) {
            super(itemView);
            container = itemView;
            levelPosition = (TextView)itemView.findViewById(R.id.levelPosition);
            levelTitle = (TextView)itemView.findViewById(R.id.levelTitle);
            viewRewards = (TextView)itemView.findViewById(R.id.viewRewards);
            pointsToUnlockValue = (TextView)itemView.findViewById(R.id.pointsToUnlockValue);
            pointsToUnlockLabel = (TextView)itemView.findViewById(R.id.pointsToUnlockLabel);
            scratchesPerDayValue = (TextView)itemView.findViewById(R.id.scratchesPerDayValue);
            scratchesPerDayLabel = (TextView)itemView.findViewById(R.id.scratchesPerDayLabel);
            unlockedView = itemView.findViewById(R.id.unlockedView);
            lockedView = itemView.findViewById(R.id.lockedView);
            line = itemView.findViewById(R.id.line);
            lineFirstItem = itemView.findViewById(R.id.lineFirstItem);
            lineLastItem = itemView.findViewById(R.id.lineLastItem);
        }
    }

}
