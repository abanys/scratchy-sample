package net.banys.scratchysample.levels;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.squareup.otto.Bus;
import com.squareup.otto.Subscribe;

import net.banys.scratchysample.R;
import net.banys.scratchysample.bus.BusProvider;
import net.banys.scratchysample.bus.event.LevelListAvailableEvent;
import net.banys.scratchysample.model.Level;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * fragment shows list of levels
 * Created by adam on 22.12.15.
 */
public class LevelListFragment extends Fragment {

    @Bind(R.id.levelRecycler) RecyclerView levelListRecycler;

    private Bus mBus = BusProvider.getInstance();
    private LevelListAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_level_list, container, false);
        ButterKnife.bind(this, v);
        return v;
    }

    @Override public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        mBus.register(this);
    }

    @Override
    public void onPause() {
        mBus.unregister(this);
        super.onPause();
    }

    @Subscribe
    public void onLevelsAvailable(LevelListAvailableEvent event) {
        prepareLevelList(event.getLevels());
    }

    private void prepareLevelList(List<Level> levels) {
        if (mAdapter == null) {
            mAdapter = new LevelListAdapter(getContext().getApplicationContext(), levels);
            levelListRecycler.setLayoutManager(new LinearLayoutManager(getContext().getApplicationContext(),
                    LinearLayoutManager.VERTICAL, true));
            levelListRecycler.setAdapter(mAdapter);
        }
    }
}
