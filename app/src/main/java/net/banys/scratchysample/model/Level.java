package net.banys.scratchysample.model;

import net.banys.scratchysample.R;

/**
 * Created by adam on 22.12.15.
 */
public class Level {

    private int id;
    private long pointsToUnlock;
    private int scratchesPerDay;
    private boolean unlocked;
    private int background;

    public Level(int id, long pointsToUnlock, int scratchesPerDay, boolean unlocked, int background) {
        this.pointsToUnlock = pointsToUnlock;
        this.id = id;
        this.scratchesPerDay = scratchesPerDay;
        this.unlocked = unlocked;
        this.background = background;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getPointsToUnlock() {
        return pointsToUnlock;
    }

    public void setPointsToUnlock(long pointsToUnlock) {
        this.pointsToUnlock = pointsToUnlock;
    }

    public int getScratchesPerDay() {
        return scratchesPerDay;
    }

    public void setScratchesPerDay(int scratchesPerDay) {
        this.scratchesPerDay = scratchesPerDay;
    }

    public boolean isUnlocked() {
        return unlocked;
    }

    public void setUnlocked(boolean unlocked) {
        this.unlocked = unlocked;
    }

    public int getBackground() {
        return background;
    }
}
