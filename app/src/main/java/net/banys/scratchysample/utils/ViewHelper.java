package net.banys.scratchysample.utils;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;

/**
 * Created by adam on 23.12.15.
 */
public class ViewHelper {

    public static void setBackgroundShapeColor(Context context, View view, int color) {
        Drawable background = view.getBackground();
        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable)background).getPaint()
                    .setColor(ContextCompat.getColor(context, color));
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable)background).setColor(ContextCompat.getColor(context, color));
        }
    }

}
